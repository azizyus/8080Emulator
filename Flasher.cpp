//
// Created by azizyus on 5/16/19.
//

    #include "Flasher.h"
    #include "ROM.h"

    void Flasher::flash(State8080 *mcu, unsigned char *bytes, int space, int size)
    {


        for (int i = 0; i < size; ++i)
        {
            mcu->memory[space + i] = bytes[i]; //load file(program) after 0x100
        }
        mcu->pc = space; //the address where cpm should start processsing

    }



    void Flasher::flashFile(State8080 *mcu, int argc, char **argv)
    {
        ROM *gameFile = this->getGameFile(argc,argv);
        this->flash(mcu,gameFile->gameBytes,0x0,gameFile->size);
    }


    void Flasher::flashForCPM(State8080 *mcu,unsigned char *bytes, int size)
    {
        mcu->memory[5] = 0xC9; //i cant remember but probably somethign neccesary
        this->flash(mcu, bytes, 0x100, size);
    }


    ROM* Flasher::getGameFile(int argc,char **argv)
    {

        string fileName = "";
        if(argc >= 2) fileName = argv[1];
        else printf("please provide file name \n");
        FILE *f = fopen(fileName.c_str(),"rb");
        if(f==NULL)
        {
            printf("CANT OPEN FILE");
            exit(1);
        }

        fseek(f,0L,SEEK_END); //send pointer to end of file
        size_t fsize= ftell(f); //get position of pointer
        fseek(f,0L,SEEK_SET); //send pointer to beginning of file

        unsigned char buffer[fsize]; //alloc memory as file size
        fread(buffer,fsize,1,f);//read file and fill to buffer
        fclose(f); //close file so other things can read that file too

        return new ROM(buffer,fsize,fileName);

    }

    void Flasher::flashFileForCPM(State8080 *mcu,int argc, char **argv)
    {

        ROM *gameFile = this->getGameFile(argc,argv);
        this->flashForCPM(mcu,gameFile->gameBytes,gameFile->size);

    }
