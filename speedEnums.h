
const long _MHZ_MULTIPLIER = 1000000;
const long _GHZ_MULTIPLIER = 10000000;
const long _KHZ_MULTIPLIER = 1000;

const long KHZ500 = 500 * _KHZ_MULTIPLIER;
const long MHZ1 = 1 * _MHZ_MULTIPLIER;
const long MHZ2 = 2 * _MHZ_MULTIPLIER;
const long MHZ32 = 32 * _MHZ_MULTIPLIER;
const long GHZ1 = 1 * _GHZ_MULTIPLIER;
const long GHZ2 = 2 * _GHZ_MULTIPLIER;
const long GHZ3 = 3 * _GHZ_MULTIPLIER;


