#include "dad_test.h"
#include "dcx_test.h"
#include "inr_test.h"
#include "dcr_test.h"
#include "cma_test.h"
#include "dcx_single_test.h"
#include "acc_flag_test.h"
class allTests
{

public:

    static void runAll(State8080 *state)
    {

        dadTest::test(state);
        dcx_test::dcxTest(state);
        inr_test::inrTest(state);
        dcr_test::dcrTest(state);
        cmaTest::test(state);
        dcxSingleTest::test(state);
        acc_flag_test::test(state);



        printf("\n ALL TEST OK! \n");
        exit(0);
    }


};