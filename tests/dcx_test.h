

class dcx_test
{



public:

    static void dcxTest(State8080 *state)
    {


        state->h = 0x98;
        state->l = 0x00;

        uint16_t result = state->dcxCode(&state->h,&state->l);

        if(state->combine8RegisterPairs(state->h,state->l) == 0x97FF) printf("DCX TEST OK! \n");
        else
        {
            printf("ERROR ON DCX TEST");
            exit(1);
        }
    }

};