
using namespace std;
#ifndef INC_8080EMULATOR_STATE8080_H
#define INC_8080EMULATOR_STATE8080_H

#include <iostream>
#include <unistd.h>
#include "chrono"
#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include "speedEnums.h"





class State8080{

public:
    int currentCycles = 0;
    int lastCurrentCycles = 0;
    int lastCurrentCycleDifference = 0;
    int currentFreq = GHZ2;

    struct ConditionCodes {
        uint8_t    z=0;
        uint8_t    s=0;
        uint8_t    p=0;
        uint8_t    cy=0;
        uint8_t    ac=0;
        uint8_t    pad:3;
        uint8_t ie=0;
    } ConditionCodes;

    uint8_t a;
    uint8_t b;
    uint8_t c;
    uint8_t d;
    uint8_t e;
    uint8_t h;
    uint8_t l;
    uint16_t sp;
    uint16_t pc=0;
    struct ConditionCodes cc;
    uint8_t memory[640000];


    void affectCarryBit(int result)
    {
        //printf("Carry RESULT IS [%i]",result);
        if(result > 0xFF || result < 0)
        {
            this->cc.cy = 1;
        }
        else this->cc.cy = 0;
        //printf(" CARRIY IS [%i]",this->cc.cy);
    }


    void affectZeroBit(u_int32_t result)
    {
     //   printf("ZEROBIT RESULT IS  [%i]",result);
        if(result==0) this->cc.z = 1;
        else this->cc.z = 0;
     //   printf("ZERO FLAG IS [%i]",this->cc.z);
    }

    void affectSignBit(int result)
    {

     //   printf("\n");
      //  printf("SIGNBIT RESULT IS[%i]",result);
        int8_t seventhBit = (result & 0x80) >> 7;
        this->cc.s = seventhBit;
       // printf("SIGNBIT RESULT IS[%i]",seventhBit);
    //    printf("\n");
    }

    void affectParity(int x)
    {
        this->cc.p = 1;
        for(uint8_t i = 0; i < 8; i++)
        {
            this->cc.p ^= (x & 1 << i) >> i;
        }
    }

    void affectAuxiliary(int result)
    {

        if(result>15 || result<0)
        {
            this->cc.ac=1;
        }
        else
        {
            this->cc.ac=0;
        }


    }


    void resetFlags()
    {

        this->cc.z = 0;
        this->cc.s = 0;
        this->cc.p = 0;
        this->cc.cy = 0;
        this->cc.ac = 0;
        this->cc.pad = 0;
        this->cc.ie = 0;

    }

    int countBit1Fast(int n)
    {

        if(n<0) n = n*-1;
        int c = 0;
        for (; n; ++c)
            n &= n - 1;
        return c;
    }


    void inx(uint8_t *firstRegisterPair, uint8_t *secondRegisterPair)
    {

        uint16_t combinedRegisterPair = this->combine8RegisterPairs(*firstRegisterPair,*secondRegisterPair);
        combinedRegisterPair++;


        *firstRegisterPair = this->getLeft8Bit(combinedRegisterPair);
        *secondRegisterPair = this->getRight8Bit(combinedRegisterPair);
        this->currentCycles += 5;
    }


    //DOUBLE CHECK -
    void inr(uint8_t *myRegister)
    {

        //255 + 1 makes 0 in 1byte so i need to calculate it as 1 byte
        uint8_t result = *myRegister + 1;

        this->affectZeroBit(result); //check is zero
        this->affectSignBit(result); //8bit is 8bit it gets 7th bit of byte
        this->affectParity(result); //8bit parity
        this->cc.ac = (result & 0x0F) == 0;
        *myRegister = result & 0xFF;
        this->currentCycles += 5;
    }

    void inr16Bit(uint8_t *myRegister)
    {
        this->currentCycles += 5; //+5 comes from normal inr function
        this->inr(myRegister);
    }

    int dcr(uint8_t *myRegister,uint8_t minus=1)
    {


        int result = *myRegister - minus;
        //printf("[%i]",result);
        this->affectZeroBit(result);
        this->affectSignBit(result);
        this->affectParity(result & 0xFF);
        this->cc.ac = !((result & 0x0F) == 0x0F);

        *myRegister = result & 0xFF;
        this->currentCycles += 5;
        return result;
    }

    int dcr16Bit(uint8_t *myRegister,uint8_t minus=1)
    {
        this->currentCycles += 5;
        return this->dcr(myRegister,minus); //and +5 comes from here
    }


    uint8_t getByte(unsigned char *opcode)
    {
        return opcode[1];
    }




//    Carry Flag is a flag set when:
//
//            a) two unsigned numbers were added and the result is larger than "capacity" of register
//            where it is saved. Ex: we wanna add two 8 bit numbers and save result in 8 bit register.
//            In your example: 255 + 9 = 264 which is more that 8 bit register can store. So the value "8"
//            will be saved there (264 & 255 = 8) and CF flag will be set.
//


    uint8_t getRight4Bit(uint8_t value)
    {
        return value & 0x0F;
    }

    uint8_t getLeft4Bit(uint8_t value)
    {
        return (value & 0xF0)>>4;
    }


    // no need to count cycles here because of add(); method
    void daa()
    {



        uint8_t leastSigBits = this->getRight4Bit(this->a);
        uint8_t mostSigBits = this->getLeft4Bit(this->a);
        uint8_t valToAdd= 0;
        uint8_t oldCy = this->cc.cy;
        if(leastSigBits>9 || this->cc.ac == 1)
        {
            valToAdd+=0x06; //or this->a = this->a || 0000 0110

        }

        //      printf("---- [[[[A IS [%02x]]]]]",this->a);


        if((mostSigBits>=9 && leastSigBits>9) || mostSigBits > 9 || this->cc.cy == 1)
        {
            valToAdd += 0x60; //its 0110 0000
            oldCy=1;
        }


//        this->affectZeroBit(this->a);
//        this->affectSignBit(this->a);
        this->add(&this->a,valToAdd,0);

        this->cc.cy = oldCy;

    }



    void rar()
    {
        //A = A >> 1; bit 7 = prev bit 7; CY = prev bit 0

        uint8_t tempAPrevBit0 = this->a << 7;
        this->a = this->a >> 1;
        this->a = this->a | (this->cc.cy << 7);
        this->cc.cy = tempAPrevBit0 >> 7;
        this->currentCycles += 4;
    }

    void ral()
    {

        //A = A << 1; bit 0 = prev CY; CY = prev bit 7
        uint8_t tempAPrevBit7 = this->a >> 7;
        this->a = this->a << 1;
        this->a = this->a | this->cc.cy;
        this->cc.cy = tempAPrevBit7;
        this->currentCycles += 4;


    }

    void rlc()
    {

//        this->a=0xF2;
        //A = A << 1; bit 0 = prev bit 7; CY = prev bit 7
        uint8_t tempAPrevBit7 = this->a >> 7;
        this->cc.cy = tempAPrevBit7;
        this->a = this->a << 1;
        this->a = this->a | tempAPrevBit7;
        this->currentCycles += 4;

//        this->printFlags();
//        this->printRegisters();
//        exit(0);
    }

    void rrc()
    {
        //A = A >> 1; bit 7 = prev bit 0; CY = prev bit 0
        uint8_t tempAPrevBit0 = this->a << 7;
        this->cc.cy = tempAPrevBit0 >> 7;
        this->a = this->a >> 1;
        this->a = this->a | tempAPrevBit0;
        this->currentCycles += 4;
    }

    uint8_t cma()
    {

        this->a = ~this->a;
        this->currentCycles += 4;
        return this->a;
    }

    void cmc()
    {
        if(this->cc.cy == 1) this->cc.cy = 0;
        else if(this->cc.cy == 0) this->cc.cy = 1;

        this->currentCycles += 4;
    }

    uint16_t dcxSingle(uint16_t *value)
    {

        *value-=1;
        this->currentCycles += 5;
        return *value;
    }

    void dcxCode(uint8_t *firstRegisterPair,uint8_t *secondRegisterPair)
    {


        uint16_t result = this->combine8RegisterPairs(*firstRegisterPair,*secondRegisterPair);
        result--;
        *firstRegisterPair = this->getLeft8Bit(result);
        *secondRegisterPair = this->getRight8Bit(result);
        this->currentCycles += 5;


    }

    uint16_t combine8RegisterPairs(uint8_t x,uint8_t y)
    {
        uint16_t combined = (x << 8) | y;
        return combined;
    }


    bool condition(int value){
        if(value == 1)return true;
        else return false;
    }

    uint16_t combineInstructors(unsigned char *opcode)
    {
        uint16_t combined = (opcode[2] << 8) | opcode[1];
        return combined;
    }

    uint16_t getWord(unsigned char *opcode)
    {
        uint16_t address = this->combineInstructors(opcode);
        return  address;
    }

    uint8_t getLeft8Bit(uint16_t value)
    {
        return (uint8_t )((value) >> 8);
    }

    uint8_t getRight8Bit(uint16_t value)
    {
        return (uint8_t )(((value) << 8) >> 8);
    }


    void shld(unsigned char *opcode)
    {

        uint16_t addressLow = this->getWord(opcode);
        uint16_t addressHigh = addressLow;
        addressHigh++;

        this->memory[addressLow] = this->l;
        this->memory[addressHigh] = this->h;

        this->currentCycles += 16;
    }

    void lhld(unsigned char *opcode)
    {


        uint16_t addressLow = this->getWord(opcode);
        uint16_t addressHigh = addressLow;
        addressHigh++;

        this->l = this->memory[addressLow];
        this->h = this->memory[addressHigh];
        this->currentCycles += 16;
    }

    void dadCode(uint16_t bit16Total)
    {

        int myTotal = bit16Total + this->hlMAddress();
        bit16Total+= this->hlMAddress();

        this->h = this->getLeft8Bit(bit16Total);
        this->l = this->getRight8Bit(bit16Total);
        if(myTotal>0xFFFF || myTotal < 0)
        {
            this->cc.cy = 1;
        }
        else this->cc.cy=0;

    }


    void cmp(uint8_t byte)
    {
        this->subPerform(this->a,byte,0);
    }

    void cmp16Bit(uint8_t byte)
    {
        this->subPerform16Bit(this->a,byte,0);
    }

    uint8_t shift1;
    uint8_t shift0;
    uint8_t shift_offset;


    uint8_t port1;


    void in(uint8_t opcode)
    {
        uint8_t val = 0x00;
        switch(opcode)
        {
            case 1:
//                printf("PORT1 IS %d \n",port1);
                this->a = port1;
//                sleep(1);

            break;
        }
        this->currentCycles += 10;
    }
    void out(uint8_t port)
    {
        this->currentCycles += 10;
        return;
    }


    bool interrupt(int interruptEnum)
    {

            if (this->cc.ie == 1)
            {


                uint16_t opcodeAddress = (uint16_t) (8 * interruptEnum);

                this->push(this->pc);
                this->jmp(opcodeAddress);

//                this->callCurrent(opcodeAddress);
//                printf("INTERRUPT \n");

                this->cc.ie=0;
//printf("\nHERE\n");
//sleep(1);
                return true;
            }

        return false;

    }

    uint8_t getZflag()
    {
        return this->cc.z;
    }

    uint16_t calculatedReturnPoint=0;
    uint16_t pop()
    {
        uint8_t secondValue = this->memory[this->sp];
        this->sp++;
        uint8_t firstValue  = this->memory[this->sp];
        this->sp ++;
        uint16_t calculatedReturnPoint = this->combine8RegisterPairs(firstValue,secondValue);
        this->calculatedReturnPoint = calculatedReturnPoint;

        return calculatedReturnPoint;
    }

    uint16_t popForOpcode()
    {

        this->currentCycles += 10;
        return  this->pop();
    }

    uint16_t returnPoint=0;
    void push(uint16_t data)
    {

        this->sp--;
        this->memory[this->sp] = this->getLeft8Bit(data);
        this->sp--;
        this->memory[this->sp] = this->getRight8Bit(data);

        this->returnPoint = data;

    }

    void pushForOpcode(uint16_t data)
    {
        this->push(data);
        this->currentCycles += 11;
    }

    void ret()
    {
        this->pc = this->pop();
    }

    void retForOpcode()
    {
        this->ret();
        this->currentCycles += 10;
    }


    void callForOpcode(unsigned char *opcode)
    {
        this->call(this->getWord(opcode));
        this->currentCycles += 17;
    }



    void call(uint16_t addr)
    {
        this->push(this->pc+3); //+11 cycle is here
        this->jmp(addr); //jmp uses 10 cycles
        //11 + 10 = 21 but call says its 17 SOOO :D
    }



    void jmp(uint16_t data)
    {
        this->pc = data;

    }

    void jmpForOpcode(uint16_t data)
    {
        this->jmp(data);
        this->currentCycles += 10;
    }


    void setHL(uint16_t data)
    {

        this->h = this->getLeft8Bit(data);
        this->l = this->getRight8Bit(data);

    }


    void setDE(uint16_t data)
    {

        this->d = this->getLeft8Bit(data);
        this->e = this->getRight8Bit(data);

    }


    void setBC(uint16_t data)
    {

        this->b = this->getLeft8Bit(data);
        this->c = this->getRight8Bit(data);

    }


    void popPsw()
    {
//        this->resetFlags();

        uint16_t pop = this->pop();

        uint8_t memoryViaSp = this->getRight8Bit(pop);
//        memoryViaSp = 0xC3;

        uint8_t psw = memoryViaSp & 0xFF;

        this->cc.s = (memoryViaSp & 128) >> 7;
        this->cc.z = (memoryViaSp & 64) >> 6;
        this->cc.ac = (memoryViaSp & 16) >> 4;
        this->cc.p = (memoryViaSp & 4) >> 2;
        this->cc.cy = (memoryViaSp & 1);
//        this->printFlags();
//        printf("----YOUR NEW PSW IS [%02x]",this->getPsw());
//        printf("----YOUR OLD PSW IS [%02x]",memoryViaSp);


        this->a = this->getLeft8Bit(pop);
        this->currentCycles += 10;
//        printf("[%04x]",this->memory[this->sp+1]);

//        printf("[%04x]",this->sp+1);
//        exit(0);
//        exit(0);
    }

    uint8_t getPsw()
    {

        uint8_t psw=0;
        psw |= (this->cc.s << 7);

        psw |= (this->cc.z << 6);

        psw |= (this->cc.ac << 4);

        psw |= (this->cc.p << 2);

        psw |= (this->cc.cy);

        psw |= 2;

        psw &= ~8;

        psw &= ~32;

        return psw;

    }

    void sta(unsigned char *opcode)
    {
        this->memory[this->getWord(opcode)] = this->a;
        this->currentCycles += 13;
    }


    void lda(unsigned char *opcode)
    {
        this->a =  this->memory[this->getWord(opcode)];
        this->currentCycles += 16;
    }



    void generalROpcodes(uint8_t value,uint8_t equality)
    {
       if(value == equality)
       {
           this->ret();
       }
    }


    void printFlags()
    {
        printf(" -  CY:%i P:%i S:%i  Z:%i AC:%i",
               this->cc.cy,
               this->cc.p,
               this->cc.s,
               this->cc.z,
               this->cc.ac
        );
    }





    bool isZBitZero()
    {
        if(this->cc.z==0) return true;
        else return false;
    }

    void resetSBit()
    {
        this->cc.s=0;
    }



    uint16_t hlMAddress() {

        uint16_t  hlMAddress = (this->h << 8) | this->l;
        return hlMAddress;

    }
    uint8_t getFromHLM()
    {
        uint16_t  hlMAddress = this->hlMAddress();
        return this->memory[hlMAddress];
    }

    uint16_t deAddress()
    {
        uint16_t deAddress = (this->d << 8) | this->e;
        return deAddress;
    }

    uint8_t getFromDE()
    {
        uint16_t  deAddress = this->deAddress();
        return this->memory[deAddress];
    }


    uint16_t bcAddress()
    {
        uint16_t bcAddress = (this->b << 8) | this->c;
        return bcAddress;
    }

    uint8_t getFromBC()
    {
        uint16_t bcAddress = this->bcAddress();
        return this->memory[bcAddress];
    }


    uint8_t getCyFlag()
    {
        return this->cc.cy;
    }

    void ora(uint8_t value)
    {
        uint8_t result = this->a | value;
        this->cc.ac = 0;
        this->cc.cy = 0;
        this->affectZeroBit(result);
        this->affectSignBit(result);
        this->affectParity(result);
        this->a = result;
        this->currentCycles += 4;
    }

    void ora16Bit(uint8_t value)
    {
        this->currentCycles += 3;
        this->ora(value);
    }

    /*

     There is some difference in the handling of the auxiliary carry flag by the logical AND instructions In the 8080
     processor and the 8085 processor. The 8085 logical AND instructions always set the auxiliary flag ON. The 8080 logical
     AND instructions set the flag to reflect the logical OR of bit 3 of the values involved in the AND operation.

     */
    uint8_t ana(uint8_t value)
    {
        uint8_t result = this->a & value;


        this->cc.cy = 0;
        this->cc.ac = ((this->a | value) & 0x08)>>3;
        this->affectZeroBit(result);
        this->affectSignBit(result);
        this->affectParity(result);

        this->a = result;
        this->currentCycles += 4;
        return result;
    }

    uint8_t ana16Bit(uint8_t value)
    {
        this->currentCycles += 3;
        return this->ana(value);
    }

    void ani(int value)
    {
        this->currentCycles += 3;
        uint8_t result = this->ana(value);
    }

    void affectCarrySubAdd(u_int32_t result)
    {
        result = result & 0x100;
        uint8_t cyFlag = result>>8;

        this->cc.cy = cyFlag;


    }

    uint8_t subPerform(u_int32_t registerPointer, u_int32_t byte, uint8_t carry)
    {

        int newByte = ~byte+1 - carry;
        int integerResult = registerPointer + newByte ;
        uint8_t result = registerPointer + newByte ;
        this->affectCarrySubAdd(integerResult);
        this->affectSignBit(result);
        this->affectZeroBit(result);
        this->affectParity(result);
        this->cc.ac = (~(registerPointer ^ byte ^ integerResult ) & 0x10)>>4;
        this->currentCycles += 4;
        return integerResult & 0xFF;

    }


    uint8_t subPerform16Bit(u_int32_t registerPointer, u_int32_t byte, uint8_t carry)
    {
        this->currentCycles += 3;
        return this->subPerform(registerPointer,byte,carry);

    }

    void add(uint8_t *registerPointer,u_int32_t value, uint8_t cyFlagValue)
    {
        uint8_t result = *registerPointer + value + cyFlagValue;
        int integerResult = *registerPointer + value + cyFlagValue;
        this->affectCarrySubAdd(integerResult);
        this->affectSignBit(result);
        this->affectZeroBit(result);
        this->affectParity(result);
        this->cc.ac = ((*registerPointer ^ result ^ value) & 0x10)>>4;
        *registerPointer = result;

        this->currentCycles += 4;
    }
    void add16Bit(uint8_t *registerPointer,u_int32_t value, uint8_t cyFlagValue)
    {

        this->currentCycles += 3;
        this->add(registerPointer,value,cyFlagValue);
    }

    void sbi(uint8_t registerPointer, uint8_t byte, uint8_t cyFlag)
    {
        this->a = this->subPerform(registerPointer,byte,cyFlag);
    }

    void sui(uint8_t registerPointer, uint8_t byte)
    {
        this->a = this->subPerform(registerPointer,byte,0);
    }

    void sub(uint8_t registerPointer, uint8_t byte)
    {
        this->a = this->subPerform(registerPointer, byte, 0);
    }

    void sbb(uint8_t registerPointer, uint8_t byte, uint8_t carry)
    {
        this->a = this->subPerform(registerPointer,byte,carry);
    }


    void xthl()
    {
        uint16_t tempStackPointer = this->sp;
        uint8_t tempL = this->l;
        this->l = this->memory[tempStackPointer];
        this->memory[tempStackPointer] = tempL;

        uint8_t tempH = this->h;
        this->h = this->memory[tempStackPointer+1];
        this->memory[tempStackPointer+1] = tempH;
        this->currentCycles += 18;
    }

    void xchg()
    {

        uint8_t tempH = this->h;
        uint8_t tempL = this->l;

        uint8_t tempD = this->d;
        uint8_t tempE = this->e;


        this->h = tempD;
        this->l = tempE;

        this->d = tempH;
        this->e = tempL;

        this->currentCycles += 5;

    }

    void lxi(uint8_t  *firstRegister,uint8_t *secondRegister,unsigned char *opcode)
    {
        uint16_t twoByteData = this->getWord(opcode);
        *firstRegister = this->getLeft8Bit(twoByteData);
        *secondRegister = this->getRight8Bit(twoByteData);

        this->currentCycles += 10;
    }

    void mvi(uint8_t *registerPointer, unsigned char *opcode)
    {

        *registerPointer = this->getByte(opcode);
        this->currentCycles += 7;
    }

    void mvi16Bit(uint8_t *registerPointer, unsigned char *opcode)
    {
        this->currentCycles += 3;
        this->mvi(registerPointer,opcode);
    }

    void xra(uint8_t secondValue)
    {
        uint8_t result = this->a ^ secondValue;

        this->cc.ac = 0;
        this->cc.cy = 0;
        this->affectZeroBit(result);
        this->affectSignBit(result);
        this->affectParity(result);

        this->currentCycles += 4;
        this->a = result;

    }

    void xra16Bit(uint8_t secondValue)
    {
        this->currentCycles += 3;
        this->xra(secondValue);
    }

    void notImplementedOpcode(string name)
    {
        printf(" YOU DIDNT IMPLEMENT THIS ONE ");
        cout << name;
        exit(0);
    }

    bool anyThingLeftToRead = false;
    void bdos()
    {
      //  this->printRegisters(); printf("\n");
        if(this->c == 1)
        {
            char n;
            cin.get(n);
            this->a = n;

        }
        else if(this->c == 11) //0x0B
        {

            if(!this->anyThingLeftToRead)
            {
                this->a = 0;
            }
            else this->a = 1;

        }
        else if(this->c == 2)
        {
            if(this->e!=0)
            {
                putchar((char)this->e);
            }

        }
        else if(this->c==9)
        {
            for (int addr = ((this->d << 8) | this->e); this->memory[addr] != '$';addr++) {
                if (this->memory[addr] != 0) {
                    putchar((char) this->memory[addr]);
                }

            }
           // printf("\n");
        }




    }


    void ldax()
    {
        this->a = this->memory[this->deAddress()];

    }

    void stax()
    {
        this->memory[this->deAddress()] = this->a;

        this->currentCycles += 7;
    }

    void rst()
    {
        this->currentCycles += 11;
    }


    void printRegisters()
    {
        printf(" - [A:%02X] [B:%02x] [C:%02x] [H:%02X] [L:%02X] [D:%02x] [E:%02x]  [SP:%04x] [PC:%04x] [RETPOINT:[%04X] CALCRETPOINT[%04x]",this->a,this->b,this->c,this->h,this->l,this->d,this->e,this->sp,
                this->pc,this->returnPoint,this->calculatedReturnPoint);

    }


    /**
     *
     * CONDITIONAL JUMPS WILL BE PLACED HERE
     *
     *
     */



    //Z CONDITIONED OPCODES
     uint8_t rnz()
     {

        if(this->cc.z == 0)
        {
            this->ret();
            this->currentCycles += 11;
            return 0;
        }
        else
        {
            this->currentCycles +=5;
            return 1;
        }
     }
     uint8_t jnz(unsigned char *opcode)
     {

         this->currentCycles += 10;
         if(this->cc.z == 0)
         {
             this->jmp(this->getWord(opcode)); //jmp do that +10 thing it self

             return 0;
         }
         else
         {
             return 3;
         }
     }
     uint8_t cnz(unsigned char *opcode)
     {
         if(this->cc.z == 0)
         {
             this->call(this->getWord(opcode));
             this->currentCycles += 17;
             return 0;
         }
         else
         {
             this->currentCycles += 11;
             return 3;
         }
     }

     uint8_t rz(unsigned char *opcode)
     {
         if(this->cc.z == 1)
         {
             this->ret();
             this->currentCycles += 11;
             return 0;
         }
         else
         {
             this->currentCycles += 5;
             return 1;
         }
     }
     uint8_t jz(unsigned char *opcode)
     {
         this->currentCycles += 10;
         if(this->cc.z == 1)
         {
             this->jmp(this->getWord(opcode));
             return 0;
         }
         else
         {
             return 3;
         }
     }
     uint8_t cz(unsigned char *opcode)
     {

         if(this->cc.z == 1)
         {
             this->call(this->getWord(opcode));
             this->currentCycles += 17;
             return 0;
         }
         else
         {
             this->currentCycles += 11;
             return 3;
         }
     }
    //Z CONDITIONED OPCODES


    //CARRY CONDITION OPCODES
    uint8_t rnc(unsigned char *opcode)
    {
        if(this->cc.cy == 0)
        {
            this->ret();
            this->currentCycles += 11;
            return 0;
        }
        else
        {
            this->currentCycles += 5;
            return 1;
        }
    }
    uint8_t cnc(unsigned char *opcode)
    {
        if(this->cc.cy == 0)
        {
            this->call(this->getWord(opcode));
            this->currentCycles += 17;
            return 0;
        }
        else
        {
            this->currentCycles += 11;
            return 3;
        }
    }
    uint8_t jnc(unsigned char *opcode)
    {
         this->currentCycles += 10;
        if(this->cc.cy == 0)
        {
            this->jmp(this->getWord(opcode));
            return 0;
        }
        else
        {
            return 3;
        }
    }

    uint8_t rc(unsigned char *opcode)
    {
        if(this->cc.cy == 1)
        {
            this->ret();
            this->currentCycles += 11;
            return 0;
        }
        else
        {
            this->currentCycles += 5;
            return 1;
        }
    }
    uint8_t jc(unsigned char *opcode)
    {
         this->currentCycles += 10;
        if(this->cc.cy == 1)
        {
            this->jmp(this->getWord(opcode));
            return 0;
        }
        else
        {
            return 3;
        }
    }
    uint8_t ccMethod(unsigned char *opcode)
    {
        if(this->cc.cy == 1)
        {
            this->call(this->getWord(opcode));
            this->currentCycles += 17;
            return 0;
        }
        else
        {
            this->currentCycles += 11;
            return 3;
        }
    };
    //CARRY CONDITION OPCODES



    //PARITY CONDITION OPCODES
    uint8_t rpo()
    {
        if(this->cc.p == 0)
        {
            this->ret();
            this->currentCycles += 11;
            return 0;
        }
        else
        {
            this->currentCycles += 5;
            return 1;
        }
    }
    uint8_t cpo(unsigned char *opcode)
    {
        if(this->cc.p == 0)
        {
            this->call(this->getWord(opcode));
            this->currentCycles += 17;
            return 0;
        }
        else
        {
            this->currentCycles += 11;
            return 3;
        }
    }
    uint8_t jpo(unsigned char *opcode)
    {
        this->currentCycles += 10;
        if(this->cc.p == 0)
        {
            this->jmp(this->getWord(opcode));
            return 0;
        }
        else
        {
            return 3;
        }
    }

    uint8_t rpe(unsigned char *opcode)
    {
        if(this->cc.p == 1)
        {
            this->ret();
            this->currentCycles += 11;
            return 0;
        }
        else
        {
            this->currentCycles += 5;
            return 1;
        }
    }
    uint8_t jpe(unsigned char *opcode)
    {
        if(this->cc.p == 1)
        {
            this->jmp(this->getWord(opcode));
            return 0;
        }
        else
        {
            return 3;
        }
    }
    uint8_t cpe(unsigned char *opcode)
    {
        if(this->cc.p == 1)
        {
            this->call(this->getWord(opcode));
            this->currentCycles += 17;
            return 0;
        }
        else
        {
            this->currentCycles += 11;
            return 3;
        }
    }
    //PARITY CONDITION OPCODES


    //SIGN CONDITION OPCODES
    uint8_t rp(unsigned char *opcode)
    {
        if(this->cc.s == 0)
        {
            this->ret();
            this->currentCycles += 11;
            return 0;
        }
        else
        {
            this->currentCycles += 5;
            return 1;
        }
    }
    uint8_t jp(unsigned char *opcode)
    {
        this->currentCycles += 10;
        if(this->cc.s == 0)
        {
            this->jmp(this->getWord(opcode));
            return 0;
        }
        else
        {
            return 3;
        }
    }
    uint8_t cp(unsigned char *opcode)
    {
        if(this->cc.s == 0)
        {
            this->call(this->getWord(opcode));
            this->currentCycles += 17;
            return 0;
        }
        else
        {
            this->currentCycles += 11;
            return 3;
        }
    }

    uint8_t rm(unsigned char *opcode)
    {
        if(this->cc.s == 1)
        {
            this->ret();
            return 0;
        }
        else
        {
            return 1;
        }
    }
    uint8_t jm(unsigned char *opcode)
    {
        this->currentCycles += 10;
        if(this->cc.s == 1)
        {
            this->jmp(this->getWord(opcode));
            return 0;
        }
        else
        {
            return 3;
        }
    }
    uint8_t cm(unsigned char *opcode)
    {
        if(this->cc.s == 1)
        {
            this->call(this->getWord(opcode));
            this->currentCycles += 17;
            return 0;
        }
        else
        {
            this->currentCycles += 11;
            return 3;
        }
    }
    //SIGN CONDITION OPCODES

    /**
 *
 * LIST OF EMPTY OPCODES
 *
 * RIM
 * IN
 * SIM
 * OUT
 */


    bool isSet=false;
    int loop = 0;
    //pc is means program counter


    void debugPrint(unsigned char *opcode,int programCounter)
    {
        printf("[%u] ([%02X %02X %02X]) [%04X]",this->loop,opcode[0],opcode[2],opcode[1],programCounter);
        //cout  << "OPCODE => " << "0x" << std::hex  <<  (int)*opcode;
        printf(" ");

    }


    void notImplementedOpcode(unsigned char *opcode)
    {
        printf(" YOU DIDNT IMPLEMENT THIS ONE ");
        printf("[%04X]",opcode[0]);
        exit(0);
    }




    void processNextOpcode()
    {


        int size = 1;

        this->lastCurrentCycleDifference = this->currentCycles - this->lastCurrentCycles;
        this->lastCurrentCycles = this->currentCycles;

        unsigned char *opcode = &this->memory[this->pc];


#ifdef PRINT_DEBUG
        debugPrint(opcode,this->pc);
#endif

        if(this->isSet)
        {
            debugPrint(opcode,this->pc);
        }

        //MOV r1,M  Copies the contents of the byte addressed by the (HL) register pair to r1.
        //it says ADDRESSED BY HL that means basically merge these bits and search it on memory
        //its feels like pointer in 8080

        uint16_t  hlMAddress = this->hlMAddress();
        uint16_t  deAddress = this->deAddress();
        uint16_t bcAddress = this->bcAddress();


        switch(*opcode)
        {



            case 0x07: this->myPrintf("RLC"); this->rlc(); break;
            case 0x0f: this->myPrintf("RRC"); this->rrc(); break;
            case 0x17: this->myPrintf("RAL"); this->ral(); break;
            case 0x1f: this->myPrintf("RAR"); this->rar(); break;


            case 0x08: this->myPrintf("NOP"); this->nop(); break;
            case 0x10: this->myPrintf("NOP"); this->nop(); break;
            case 0x18: this->myPrintf("NOP"); this->nop();  break;
            case 0x00: this->myPrintf("NOP"); this->nop(); break;
            case 0xfd: this->myPrintf("NOP"); this->nop();  break;
            case 0x28: this->myPrintf("NOP"); this->nop(); break;
            case 0x20: this->myPrintf("RIM/NOP"); this->nop(); break;
            case 0xed: this->myPrintf("NOP"); notImplementedOpcode(opcode); break;
            case 0xdd: this->myPrintf("NOP"); notImplementedOpcode(opcode); break;
            case 0xd9: this->myPrintf("NOP"); this->nop(); break;
            case 0x38: this->myPrintf("NOP"); this->nop(); break;
            case 0xcb: this->myPrintf("NOP"); this->nop(); break;





            case 0x12: this->myPrintf("STAX D"); this->stax(); break;
            case 0x02: this->myPrintf("STAX B"); this->memory[bcAddress] = this->a; break;
            case 0x1a: this->myPrintf("LDAX D $%02X",deAddress); this->ldax(); break;
            case 0x0a: this->myPrintf("LDAX B");  this->a = this->memory[bcAddress]; break;








            case 0x21: this->myPrintf("LXI H, D16"); this->lxi(&this->h,&this->l,opcode); size=3; break;
            case 0x31: this->myPrintf("LXI SP, D16"); this->sp = this->combineInstructors(opcode); size=3; break;
            case 0x11: this->myPrintf("LXI D, D16"); this->lxi(&this->d,&this->e,opcode); size=3; break;
            case 0x01: this->myPrintf("LXI B $%02X%02X",opcode[2],opcode[1]); this->lxi(&this->b,&this->c,opcode); size=3; break;


            case 0x27: this->myPrintf("DAA"); this->daa(); break;



            case 0x22: this->myPrintf("SHLD adr"); this->shld(opcode); size=3; break;
            case 0x2a: this->myPrintf("LHLD adr"); this->lhld(opcode); size=3; break;

            case 0x30: this->myPrintf("SIM"); break;


            case 0xd3: this->myPrintf("OUT D8"); this->out(opcode[1]); size=2; break;
            case 0xdb: this->myPrintf("IN D8"); this->in(opcode[1]); size=2; break;



            case 0x2f: this->myPrintf("CMA"); this->cma(); break;





            case 0x32: this->myPrintf("STA adr"); this->sta(opcode); size=3; break;
            case 0x3a: this->myPrintf("LDA adr"); this->lda(opcode); size=3; break;


            case 0x39: this->myPrintf("DAD SP"); this->dadCode(this->sp); break;
            case 0x19: this->myPrintf("DAD D"); this->dadCode(deAddress); break;
            case 0x09: this->myPrintf("DAD B"); this->dadCode(bcAddress); break;
            case 0x29: this->myPrintf("DAD H"); this->dadCode(hlMAddress); break;



            case 0x36: this->myPrintf("MVI M, D8"); this->mvi16Bit(&this->memory[hlMAddress],opcode); size = 2; break;
            case 0x3e: this->myPrintf("MVI A");     this->mvi(&this->a,opcode); size = 2; break;
            case 0x0e: this->myPrintf("MVI C,D8");  this->mvi(&this->c,opcode); size=2; break;
            case 0x16: this->myPrintf("MVI D, D8"); this->mvi(&this->d,opcode); size=2; break;
            case 0x06: this->myPrintf("MVI B");     this->mvi(&this->b,opcode); size=2; break;
            case 0x2e: this->myPrintf("MVI L, D8"); this->mvi(&this->l,opcode); size = 2; break;
            case 0x1e: this->myPrintf("MVI E, D8"); this->mvi(&this->e,opcode); size=2; break;
            case 0x26: this->myPrintf("MVI H, D8"); this->mvi(&this->h,opcode); size=2; break;




            case 0x3f: this->myPrintf("CMC"); this->cmc(); break;
            case 0x37: this->myPrintf("STC"); this->cc.cy = 1; break;




            case 0xf5: this->myPrintf("PUSH PSW"); this->pushForOpcode(this->combine8RegisterPairs(this->a,this->getPsw())); break;
            case 0xe5: this->myPrintf("PUSH H"); this->pushForOpcode(this->hlMAddress()); break;
            case 0xc5: this->myPrintf("PUSH B"); this->pushForOpcode(this->bcAddress()); break;
            case 0xd5: this->myPrintf("PUSH D"); this->pushForOpcode(this->deAddress()); break;



            case 0x3b: this->myPrintf("DCX SP"); this->dcxSingle(&this->sp); break;
            case 0x2b: this->myPrintf("DCX H"); this->dcxCode(&this->h,&this->l); break;
            case 0x0b: this->myPrintf("DCX B"); this->dcxCode(&this->b,&this->c); break;
            case 0x1b: this->myPrintf("DCX D"); this->dcxCode(&this->d,&this->e); break;



            case 0x23: this->myPrintf("INX H $%02X",hlMAddress ); this->inx(&this->h,&this->l); break;
            case 0x33: this->myPrintf("INX SP"); this->sp++; break;
            case 0x03: this->myPrintf("INX B"); this->inx(&this->b,&this->c); break;
            case 0x13: this->myPrintf("INX D, $%02X",deAddress); this->inx(&this->d,&this->e); break;



            case 0x3c: this->myPrintf("INR A"); this->inr(&this->a); break;
            case 0x04: this->myPrintf("INR B"); this->inr(&this->b); break;
            case 0x34: this->myPrintf("INR M"); this->inr16Bit(&this->memory[hlMAddress]); break;
            case 0x2c: this->myPrintf("INR L"); this->inr(&this->l); break;
            case 0x24: this->myPrintf("INR H"); this->inr(&this->h); break;
            case 0x1c: this->myPrintf("INR E"); this->inr(&this->e); break;
            case 0x14: this->myPrintf("INR D"); this->inr(&this->d); break;
            case 0x0c: this->myPrintf("INR C"); this->inr(&this->c); break;



            case 0x3d: this->myPrintf("DCR A"); this->dcr(&this->a,1); break;
            case 0x05: this->myPrintf("DCR B"); this->dcr(&this->b,1); break;
            case 0x2d: this->myPrintf("DCR L"); this->dcr(&this->l,1); break;
            case 0x35: this->myPrintf("DCR M"); this->dcr16Bit(&this->memory[hlMAddress],1); break;
            case 0x25: this->myPrintf("DCR H"); this->dcr(&this->h,1); break;
            case 0x15: this->myPrintf("DCR D"); this->dcr(&this->d,1); break;
            case 0x1d: this->myPrintf("DCR E"); this->dcr(&this->e,1); break;
            case 0x0d: this->myPrintf("DCR C"); this->dcr(&this->c,1); break;


            case 0x40: this->myPrintf("MOV B,B"); this->mov(&this->b,&this->b); break; //is something i cant see or just am i braindead at 3am?
            case 0x41: this->myPrintf("MOV B,C"); this->mov(&this->b,&this->c); break;
            case 0x42: this->myPrintf("MOV B,D"); this->mov(&this->b,&this->d); break;
            case 0x43: this->myPrintf("MOV B,E"); this->mov(&this->b,&this->e); break;
            case 0x44: this->myPrintf("MOV B,H"); this->mov(&this->b,&this->h); break;
            case 0x45: this->myPrintf("MOV B,L"); this->mov(&this->b,&this->l); break;
            case 0x46: this->myPrintf("MOV B,M"); this->mov(&this->b,&this->memory[this->hlMAddress()]); break;
            case 0x47: this->myPrintf("MOV B,A"); this->mov(&this->b,&this->a); break;

            case 0x48: this->myPrintf("MOV C,B"); this->mov(&this->c,&this->b); break;
            case 0x49: this->myPrintf("MOV C,C"); this->mov(&this->c,&this->c);  break;
            case 0x4a: this->myPrintf("MOV C,D"); this->mov(&this->c,&this->d);  break;
            case 0x4b: this->myPrintf("MOV C,E"); this->mov(&this->c,&this->e);  break;
            case 0x4c: this->myPrintf("MOV C,H"); this->mov(&this->c,&this->h);  break;
            case 0x4d: this->myPrintf("MOV C,L"); this->mov(&this->c,&this->l);  break;
            case 0x4e: this->myPrintf("MOV C,M"); this->mov(&this->c,&this->memory[this->hlMAddress()]);  break;
            case 0x4f: this->myPrintf("MOV C,A"); this->mov(&this->c,&this->a);  break;

            case 0x50: this->myPrintf("MOV D,B"); this->mov(&this->d,&this->b); break;
            case 0x51: this->myPrintf("MOV D,C"); this->mov(&this->d,&this->c); break;
            case 0x52: this->myPrintf("MOV D,D"); this->mov(&this->d,&this->d); break;
            case 0x53: this->myPrintf("MOV D,E"); this->mov(&this->d,&this->e); break;
            case 0x54: this->myPrintf("MOV D,H"); this->mov(&this->d,&this->h);break;
            case 0x55: this->myPrintf("MOV D,L"); this->mov(&this->d,&this->l);break;
            case 0x56: this->myPrintf("MOV D,M"); this->mov(&this->d,&this->memory[this->hlMAddress()]); break;
            case 0x57: this->myPrintf("MOV D,A"); this->mov(&this->d,&this->a); break;

            case 0x58: this->myPrintf("MOV E,B"); this->mov(&this->e,&this->b); break;
            case 0x59: this->myPrintf("MOV E,C"); this->mov(&this->e,&this->c); break;
            case 0x5a: this->myPrintf("MOV E,D"); this->mov(&this->e,&this->d);  break;
            case 0x5b: this->myPrintf("MOV E,E"); this->mov(&this->e,&this->e);  break;
            case 0x5c: this->myPrintf("MOV E,H"); this->mov(&this->e,&this->h);  break;
            case 0x5d: this->myPrintf("MOV E,L"); this->mov(&this->e,&this->l);  break;
            case 0x5e: this->myPrintf("MOV E,M"); this->mov(&this->e,&this->memory[this->hlMAddress()]);  break;
            case 0x5f: this->myPrintf("MOV E,A"); this->mov(&this->e,&this->a);  break;

            case 0x60: this->myPrintf("MOV H,B"); this->mov(&this->h,&this->b);  break;
            case 0x61: this->myPrintf("MOV H,C"); this->mov(&this->h,&this->c);  break;
            case 0x62: this->myPrintf("MOV H,D"); this->mov(&this->h,&this->d);  break;
            case 0x63: this->myPrintf("MOV H,E"); this->mov(&this->h,&this->e);  break;
            case 0x64: this->myPrintf("MOV H,H"); this->mov(&this->h,&this->h);  break;
            case 0x65: this->myPrintf("MOV H,L"); this->mov(&this->h,&this->l);  break;
            case 0x66: this->myPrintf("MOV H,M"); this->mov(&this->h,&this->memory[this->hlMAddress()]);  break;
            case 0x67: this->myPrintf("MOV H,A"); this->mov(&this->h,&this->a);  break;

            case 0x68: this->myPrintf("MOV L,B"); this->mov(&this->l,&this->b);  break;
            case 0x69: this->myPrintf("MOV L,C"); this->mov(&this->l,&this->c); break;
            case 0x6a: this->myPrintf("MOV L,D"); this->mov(&this->l,&this->d); break;
            case 0x6b: this->myPrintf("MOV L,E"); this->mov(&this->l,&this->e); break;
            case 0x6c: this->myPrintf("MOV L,H"); this->mov(&this->l,&this->h); break;
            case 0x6d: this->myPrintf("MOV L,L"); this->mov(&this->l,&this->l); break;
            case 0x6e: this->myPrintf("MOV L,M"); this->mov(&this->l,&this->memory[this->hlMAddress()]); break;
            case 0x6f: this->myPrintf("MOV L,A"); this->mov(&this->l,&this->a); break;


            case 0x70: this->myPrintf("MOV M,B"); this->mov16Bit(&this->memory[hlMAddress],&this->b); break;
            case 0x71: this->myPrintf("MOV M,C"); this->mov16Bit(&this->memory[hlMAddress],&this->c); break;
            case 0x72: this->myPrintf("MOV M,D"); this->mov16Bit(&this->memory[hlMAddress],&this->d); break;
            case 0x73: this->myPrintf("MOV M,E"); this->mov16Bit(&this->memory[hlMAddress],&this->e); break;
            case 0x74: this->myPrintf("MOV M,H"); this->mov16Bit(&this->memory[hlMAddress],&this->h); break;
            case 0x75: this->myPrintf("MOV M,L"); this->mov16Bit(&this->memory[hlMAddress],&this->l); break;
            case 0x77: this->myPrintf("MOV M,A"); this->mov16Bit(&this->memory[hlMAddress],&this->a); break;


            case 0x78: this->myPrintf("MOV A,B"); this->mov(&this->a,&this->b); break;
            case 0x79: this->myPrintf("MOV A,C"); this->mov(&this->a,&this->c); break;
            case 0x7a: this->myPrintf("MOV A,D"); this->mov(&this->a,&this->d); break;
            case 0x7b: this->myPrintf("MOV A,E"); this->mov(&this->a,&this->e); break;
            case 0x7c: this->myPrintf("MOV A,H"); this->mov(&this->a,&this->h); break;
            case 0x7d: this->myPrintf("MOV A,L"); this->mov(&this->a,&this->l); break;
            case 0x7e: this->myPrintf("MOV A,M"); this->mov(&this->a,&this->memory[this->hlMAddress()]); break;
            case 0x7f: this->myPrintf("MOV A,A"); this->mov(&this->a,&this->a);break;


            case 0x80: this->myPrintf("ADD B"); this->add(&this->a,this->b,0); break;
            case 0x81: this->myPrintf("ADD C"); this->add(&this->a,this->c,0); break;
            case 0x82: this->myPrintf("ADD D"); this->add(&this->a,this->d,0); break;
            case 0x83: this->myPrintf("ADD E"); this->add(&this->a,this->e,0); break;
            case 0x84: this->myPrintf("ADD H"); this->add(&this->a,this->h,0); break;
            case 0x85: this->myPrintf("ADD L"); this->add(&this->a,this->l,0); break;
            case 0x86: this->myPrintf("ADD M"); this->add16Bit(&this->a,this->memory[hlMAddress],0); break;
            case 0x87: this->myPrintf("ADD A"); this->add(&this->a,this->a,0); break;
            case 0xc6: this->myPrintf("ADI D8 %02X",opcode[1]); this->add16Bit(&this->a,this->getByte(opcode),0); size=2; break;



            case 0x88: this->myPrintf("ADC B"); this->add(&this->a,this->b,this->cc.cy); break;
            case 0x89: this->myPrintf("ADC C"); this->add(&this->a,this->c,this->cc.cy); break;
            case 0x8a: this->myPrintf("ADC D"); this->add(&this->a,this->d,this->cc.cy); break;
            case 0x8b: this->myPrintf("ADC E"); this->add(&this->a,this->e,this->cc.cy); break;
            case 0x8c: this->myPrintf("ADC H"); this->add(&this->a,this->h,this->cc.cy); break;
            case 0x8d: this->myPrintf("ADC L"); this->add(&this->a,this->l,this->cc.cy); break;
            case 0x8e: this->myPrintf("ADC M"); this->add16Bit(&this->a,this->memory[hlMAddress],this->cc.cy); break;
            case 0x8f: this->myPrintf("ADC A"); this->add(&this->a,this->a,this->cc.cy); break;
            case 0xce: this->myPrintf("ACI D8"); this->add16Bit(&this->a,this->getByte(opcode),this->cc.cy); size=2; break;


            case 0x90: this->myPrintf("SUB B"); this->a = this->subPerform(this->a,this->b,0); break;
            case 0x91: this->myPrintf("SUB C"); this->a = this->subPerform(this->a,this->c,0); break;
            case 0x92: this->myPrintf("SUB D"); this->a = this->subPerform(this->a,this->d,0); break;
            case 0x93: this->myPrintf("SUB E"); this->a = this->subPerform(this->a,this->e,0); break;
            case 0x94: this->myPrintf("SUB H"); this->a = this->subPerform(this->a,this->h,0); break;
            case 0x95: this->myPrintf("SUB L"); this->a = this->subPerform(this->a,this->l,0); break;
            case 0x96: this->myPrintf("SUB M"); this->a = this->subPerform16Bit(this->a,this->memory[hlMAddress],0); break;
            case 0x97: this->myPrintf("SUB A"); this->a = this->subPerform(this->a,this->a,0); break;
            case 0xd6: this->myPrintf("SUI D8"); this->a = this->subPerform(this->a,this->getByte(opcode),0); size=2; break;


            case 0x98: this->myPrintf("SBB B"); this->a = this->subPerform(this->a, this->b, this->cc.cy); break;
            case 0x99: this->myPrintf("SBB C"); this->a = this->subPerform(this->a, this->c, this->cc.cy); break;
            case 0x9a: this->myPrintf("SBB D"); this->a = this->subPerform(this->a, this->d, this->cc.cy); break;
            case 0x9b: this->myPrintf("SBB E"); this->a = this->subPerform(this->a, this->e, this->cc.cy); break;
            case 0x9c: this->myPrintf("SBB H"); this->a = this->subPerform(this->a, this->h, this->cc.cy); break;
            case 0x9d: this->myPrintf("SBB L"); this->a = this->subPerform(this->a, this->l, this->cc.cy); break;
            case 0x9e: this->myPrintf("SBB M"); this->a = this->subPerform16Bit(this->a, this->memory[hlMAddress], this->cc.cy); break;
            case 0x9f: this->myPrintf("SBB A"); this->a = this->subPerform(this->a, this->a, this->cc.cy); break;
            case 0xde: this->myPrintf("SBI D8"); this->a = this->subPerform(this->a,this->getByte(opcode),this->cc.cy); size=2; break;


            case 0xa0: this->myPrintf("ANA B"); this->ana(this->b); break;
            case 0xa1: this->myPrintf("ANA C"); this->ana(this->c); break;
            case 0xa2: this->myPrintf("ANA D"); this->ana(this->d); break;
            case 0xa3: this->myPrintf("ANA E"); this->ana(this->e); break;
            case 0xa4: this->myPrintf("ANA H"); this->ana(this->h); break;
            case 0xa5: this->myPrintf("ANA L"); this->ana(this->l); break;
            case 0xa6: this->myPrintf("ANA M"); this->ana16Bit(this->memory[hlMAddress]); break;
            case 0xa7: this->myPrintf("ANA A"); this->ana(this->a); break;
            case 0xe6: this->myPrintf("ANI D8"); this->ana16Bit(this->getByte(opcode)); size=2; break;

            case 0xa8: this->myPrintf("XRA B"); this->xra(this->b); break;
            case 0xa9: this->myPrintf("XRA C"); this->xra(this->c); break;
            case 0xaa: this->myPrintf("XRA D"); this->xra(this->d); break;
            case 0xab: this->myPrintf("XRA E"); this->xra(this->e); break;
            case 0xac: this->myPrintf("XRA H"); this->xra(this->h); break;
            case 0xad: this->myPrintf("XRA L"); this->xra(this->l); break;
            case 0xae: this->myPrintf("XRA M"); this->xra16Bit(this->memory[hlMAddress]); break;
            case 0xaf: this->myPrintf("XRA A"); this->xra(this->a); break;
            case 0xee: this->myPrintf("XRI D8");this->xra16Bit(this->getByte(opcode)); size=2; break;


            case 0xb0: this->myPrintf("ORA B"); this->ora(this->b); break;
            case 0xb1: this->myPrintf("ORA C"); this->ora(this->c); break;
            case 0xb2: this->myPrintf("ORA D"); this->ora(this->d); break;
            case 0xb3: this->myPrintf("ORA E"); this->ora(this->e); break;
            case 0xb4: this->myPrintf("ORA H"); this->ora(this->h); break;
            case 0xb5: this->myPrintf("ORA L"); this->ora(this->l); break;
            case 0xb6: this->myPrintf("ORA M"); this->ora16Bit(this->memory[hlMAddress]); break;
            case 0xb7: this->myPrintf("ORA A"); this->ora(this->a); break;
            case 0xf6: this->myPrintf("ORI D8"); this->ora16Bit(this->getByte(opcode)); size=2; break;


            case 0xb8: this->myPrintf("CMP B"); this->cmp(this->b); break;
            case 0xb9: this->myPrintf("CMP C"); this->cmp(this->c); break;
            case 0xba: this->myPrintf("CMP D"); this->cmp(this->d);; break;
            case 0xbb: this->myPrintf("CMP E"); this->cmp(this->e); break;
            case 0xbc: this->myPrintf("CMP H"); this->cmp(this->h); break;
            case 0xbd: this->myPrintf("CMP L"); this->cmp(this->l); break;
            case 0xbe: this->myPrintf("CMP M"); this->cmp16Bit(this->memory[hlMAddress]); break;
            case 0xbf: this->myPrintf("CMP A"); this->cmp(this->a); break;
            case 0xfe: this->myPrintf("CPI D8"); this->cmp16Bit(this->getByte(opcode)); size = 2; break;


            case 0xc1: this->myPrintf("POP B"); this->setBC(this->popForOpcode()); break;
            case 0xf1: this->myPrintf("POP PSW");  this->popPsw(); break;
            case 0xe1: this->myPrintf("POP H"); this->setHL(this->popForOpcode()); break;
            case 0xd1: this->myPrintf("POP D"); this->setDE(this->popForOpcode()); break;



            case 0xe3: this->myPrintf("XTHL"); this->xthl(); break;
            case 0xeb: this->myPrintf("XCHG"); this->xchg(); break;



            case 0xf9: this->myPrintf("SPHL"); this->sphl(); break;
            case 0xe9: this->myPrintf("PCHL");  this->pchl(); size = 0; break;




            case 0xc7: this->myPrintf("RST 0"); this->rst();  break;
            case 0xcf: this->myPrintf("RST 1"); this->rst();  break;
            case 0xd7: this->myPrintf("RST 2"); this->rst();  break;
            case 0xdf: this->myPrintf("RST 3");  notImplementedOpcode(opcode);  break;
            case 0xe7: this->myPrintf("RST 4");  notImplementedOpcode(opcode);   break;
            case 0xef: this->myPrintf("RST 5");  notImplementedOpcode(opcode);   break;
            case 0xf7: this->myPrintf("RST 6");  notImplementedOpcode(opcode);  break;
            case 0xff: this->myPrintf("RST 7"); this->rst(); break;
            case 0x76: this->myPrintf("HLT"); this->halt();  break;




            case 0xf3: this->myPrintf("DI"); this->setIE(0); break;
            case 0xfb: this->myPrintf("EI"); this->setIE(1); break;


            case 0xcd: this->myPrintf("CALL adr");
                this->callForOpcode(opcode); size = 0; break;
            case 0xc3: this->myPrintf("JMP"); this->jmpForOpcode(this->getWord(opcode)); size = 0; break;
            case 0xc9: this->myPrintf("RET");  this->retForOpcode(); size = 0; break;



            case 0xc0: this->myPrintf("RNZ"); size = this->rnz(); break;
            case 0xc2: this->myPrintf("JNZ"); size = this->jnz(opcode); break;
            case 0xc4: this->myPrintf("CNZ"); size = this->cnz(opcode); break;

            case 0xc8: this->myPrintf("RZ");   size = this->rz(opcode);  break;
            case 0xca: this->myPrintf("JZ adr"); size = this->jz(opcode); break;
            case 0xcc: this->myPrintf("CZ ADR"); size = this->cz(opcode); break;




            case 0xd0: this->myPrintf("RNC");     size = this->rnc(opcode); break;
            case 0xd4: this->myPrintf("CNC adr"); size = this->cnc(opcode); break;
            case 0xd2: this->myPrintf("JNC adr"); size = this->jnc(opcode); break;

            case 0xd8: this->myPrintf("RC");  size = this->rc(opcode);   break;
            case 0xda: this->myPrintf("JC adr"); size = this->jc(opcode); break;
            case 0xdc: this->myPrintf("CC adr"); size = this->ccMethod(opcode); break;






            case 0xe0: this->myPrintf("RPO");  size = this->rpo();    break;
            case 0xe4: this->myPrintf("CPO adr"); size = this->cpo(opcode); break;
            case 0xe2: this->myPrintf("JPO adr"); size = this->jpo(opcode); break;


            case 0xe8: this->myPrintf("RPE");      size = this->rpe(opcode); break;
            case 0xea: this->myPrintf("JPE adr");  size =  this->jpe(opcode); break;
            case 0xec: this->myPrintf("CPE adr");  size = this->cpe(opcode);  break;










            case 0xf0: this->myPrintf("RP");  size = this->rp(opcode); break;
            case 0xf2: this->myPrintf("JP adr");  size = this->jp(opcode); break;
            case 0xf4: this->myPrintf("CP adr");  size = this->cp(opcode);  break;

            case 0xf8: this->myPrintf("RM");  size = this->rm(opcode); break;
            case 0xfa: this->myPrintf("JM adr"); size = this->jm(opcode); break;
            case 0xfc: this->myPrintf("CM adr"); size = this->cm(opcode); break;






        }

        if(this->isSet)
        {
            this->printFlags();
            this->printRegisters();
            printf("\n");
        }





        this->myPrintf("\n ");

        this->loop++;
        this->pc += size;

    }

    std::chrono::steady_clock::time_point lastOpcodeTime = std::chrono::steady_clock::now();
    void doIneedSleep()
    {
        if(this->currentCycles >= this->currentFreq)
        {
           this->sleep(1000000);
        }
    }

    void sleep(int sleepReference=1000000)
    {
        long iNeedToSleep = sleepReference - (std::chrono::duration_cast<std::chrono::microseconds>( std::chrono::steady_clock::now() - lastOpcodeTime).count());
        lastOpcodeTime = std::chrono::steady_clock::now();
//                printf("\n cycles[%d] - SLEEP TIME: [%ld]",this->currentCycles,iNeedToSleep);

//                printf("kkk \n");

        if(iNeedToSleep >= 0)
        {
            usleep(iNeedToSleep);
                //printf("\n cycles[%d] - SLEEP TIME: [%ld]",this->currentCycles,iNeedToSleep);
        }
        this->currentCycles = 0;
        this->lastCurrentCycleDifference = 0;
        this->lastCurrentCycles = 0;
    }

    void mov(uint8_t *firstRegister,uint8_t *secondRegister)
    {
        *firstRegister = *secondRegister;
        this->currentCycles += 5;
    }
    void mov16Bit(uint8_t *firstRegister,uint8_t *secondRegister)
    {
        this->currentCycles += 2;
        this->mov(firstRegister,secondRegister);
    }

    void setIE(uint8_t value)
    {
        this->cc.ie = value;
        this->currentCycles += 4;
    }

    void sphl()
    {
        this->sp = this->hlMAddress();
        this->currentCycles+= 5;
    }

    void pchl()
    {
        this->pc = this->hlMAddress();
        this->currentCycles+= 5;
    }

    void nop()
    {
        this->currentCycles += 4;
    }

    void halt()
    {
        this->currentCycles += 7;
        printf("SYSTEM HALTED");
        exit(0);
    }

    void myPrintf(const char *format, ...)
    {

        if(this->isSet)
        {
            va_list ap;

            va_start (ap, format);
            vprintf(format, ap);
            va_end (ap);
        }

#ifdef PRINT_DEBUG
        va_list ap;

        va_start (ap, format);
        vprintf(format, ap);
        va_end (ap);
#endif

    }

};
#endif //INC_8080EMULATOR_FLASHER_H
