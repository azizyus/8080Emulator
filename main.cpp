#include <iostream>
#include <string>
#include "State8080.h"
#include <unistd.h>
#include "Flasher.h"
#include "Runner.h"
#include "ParameterParser.h"
using namespace std;


int main(int argc, char** argv)
{


    State8080 *myState = new State8080();
    ParameterParser *myParameterCatcher = new ParameterParser();
    Flasher *myFlasher = new Flasher();
    Runner *myRunner = new Runner();

    myFlasher->flashFile(myState,argc, argv);
    myParameterCatcher->parse(myState,argc,argv);
    myRunner->run(myState);

    return 0;


}
