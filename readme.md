# status

Cmake file supports darwin and linux so safe to build for your mac and it still works in m2 macbook


# installation

cmake <br>
make <br>
make install <br>


# usage

run raw program in 2ghz
<pre>emulator-8080 your_rom_file.rom -t ghz -f 2</pre>
<br>

run cpm program in 2mhz
<pre>emulator-8080-cpm your_rom_file.rom -t mhz -f 2</pre>

run invaders
<pre>emulator-8080-invaders your_invaders.rom</pre>


note: if you dont provide -t and -f the default frequency is 2mhz which is same as 8080

# where is invaders?

its in games/invaders.rom <br>

# any program i can test?

use programs/instruction_tests but some of these programs requires very long time in 2mhz as start you can try cpudiag.bin <br>
