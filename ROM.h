//
// Created by azizyus on 5/16/19.
//

#ifndef INC_8080EMULATOR_ROM_H
#define INC_8080EMULATOR_ROM_H

#include <string>
using namespace std;

class ROM {

public:
    ROM(unsigned char bytes[],int size,string fileName);
    string fileName = "";
    unsigned char *gameBytes;
    int size = 0;

};


#endif //INC_8080EMULATOR_ROM_H
